/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from '../utils/history';

import configureReducer from '../containers/Configure/reducer'
import reportReducer from "../containers/Report/reducer";
/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({

    configureReducer: configureReducer,
    reportReducer: reportReducer,
    router: connectRouter(history),
  });

  return rootReducer;
}
