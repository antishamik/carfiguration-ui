import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AppWrapper from '../../components/AppWrapper';
import Report from "../Report/Loadable";
import Configure from "../Configure/Loadable";

export default function App() {
    return (
        <AppWrapper>
            <Switch>
                <Route exact path='/report' component={Report} />
                <Route exact path='/configure' component={Configure} />
            </Switch>
        </AppWrapper>
    );
}
