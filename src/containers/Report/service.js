import 'regenerator-runtime/runtime';

import {REQUEST} from "../../utils/request";

import {setReport} from "./actions";

export const fetchReport = () => {
    return async (dispatch) => {
        function onSuccess(response) {
            dispatch(setReport(response.data))
            return true;
        }
        function onError(error) {
            return false;
        }
        try {
            const response = await REQUEST.get('/report');
            return onSuccess(response);
        } catch (error) {
            return onError(error);
        }
    };
};
