import {SET_REPORT} from "./constants";

export const initialState = {
  reports:[],
};

const reportReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_REPORT:
        return {...state, reports: action.payload };
      default:
      return state;
  }
};

export default reportReducer;
