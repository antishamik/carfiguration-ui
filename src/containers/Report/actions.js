import {SET_REPORT} from "./constants";

export function setReport(payload) {
    return {
        type: SET_REPORT,
        payload: payload,
    };
}