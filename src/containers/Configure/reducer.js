import {SET_BATTERY, SET_CAR, SET_TIRES, SET_WHEEL} from "./constants";

export const initialState = {
  cars:[],
  wheels:[],
  tires: [],
  batteries: []
};

const configureReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CAR:
        return {...state, cars: action.payload };
    case SET_WHEEL:
      return {...state, wheels: action.payload };
    case SET_BATTERY:
      return {...state, batteries: action.payload };
    case SET_TIRES:
      return {...state, tires: action.payload };
      default:
      return state;
  }
};

export default configureReducer;
