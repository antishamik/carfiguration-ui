export const SET_CAR = 'SET_CAR'
export const SET_WHEEL = 'SET_WHEEL'
export const SET_TIRES = 'SET_TIRES'
export const SET_BATTERY = 'SET_BATTERY'