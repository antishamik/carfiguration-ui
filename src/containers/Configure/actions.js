import {SET_CAR, SET_TIRES, SET_WHEEL, SET_BATTERY} from "./constants";

export function setCar(payload) {
    return {
        type: SET_CAR,
        payload: payload,
    };
}

export function setBattery(payload) {
    return {
        type: SET_BATTERY,
        payload: payload,
    };
}

export function setWheel(payload) {
    return {
        type: SET_WHEEL,
        payload: payload,
    };
}

export function setTires(payload) {
    return {
        type: SET_TIRES,
        payload: payload,
    };
}