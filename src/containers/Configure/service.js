import 'regenerator-runtime/runtime';

import {REQUEST} from "../../utils/request";

import {setBattery, setCar, setTires, setWheel} from "./actions";

export const fetchCar = () => {
    return async (dispatch) => {
        function onSuccess(response) {
            console.log('car', response.data)
            dispatch(setCar(response.data))
            return true;
        }
        function onError(error) {
            return false;
        }
        try {
            const response = await REQUEST.get('/configure/car');
            return onSuccess(response);
        } catch (error) {
            return onError(error);
        }
    };
};

export const fetchWheel = (battery) => {
    return async (dispatch) => {
        function onSuccess(response) {
            dispatch(setWheel(response.data))
            return true;
        }
        function onError(error) {
            return false;
        }
        try {
            const response = await REQUEST.get(`/configure/wheel/${battery}`);
            return onSuccess(response);
        } catch (error) {
            return onError(error);
        }
    };
};

export const fetchTire = (wheel) => {
    return async (dispatch) => {
        function onSuccess(response) {
            dispatch(setTires(response.data))
            return true;
        }
        function onError(error) {
            return false;
        }
        try {
            const response = await REQUEST.get(`/configure/tire/${wheel}`);
            return onSuccess(response);
        } catch (error) {
            return onError(error);
        }
    };
};
export const fetchBattery = () => {
    return async (dispatch) => {
        function onSuccess(response) {
            dispatch(setBattery(response.data))
            return true;
        }
        function onError(error) {
            return false;
        }
        try {
            const response = await REQUEST.get('/configure/battery');
            return onSuccess(response);
        } catch (error) {
            return onError(error);
        }
    };
};


export const save = (data) => {
    let {username, price} = data
    return async (dispatch) => {
        function onSuccess(response) {
            return true;
        }
        function onError(error) {
            return false;
        }
        try {
            const response = await REQUEST.post('/configure/save', {
                username,
                price
            });
            return onSuccess(response);
        } catch (error) {
            return onError(error);
        }
    };
};