import React, {useState, useEffect} from "react";
import { useSelector, useDispatch } from 'react-redux';
import {
    Alert,
    Box,
    Button,
    Dialog, DialogActions,
    DialogContent, DialogContentText,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from "@mui/material";
import {fetchBattery, fetchCar, fetchTire, fetchWheel, save} from "./service";

export default function Configure() {
    const dispatch = useDispatch();
    const [battery, setBattery] = useState();
    const [batteryPrice, setBatteryPrice] = useState(0);
    const [wheel, setWheel] = useState();
    const [wheelPrice, setWheelPrice] = useState(0);
    const [wheelDisable, setWheelDisable] = useState(true);
    const [tireDisable, setTireDisable] = useState(true)
    const [tirePrice, setTirePrice] = useState(0);
    const [price, setPrice] = useState(0)
    const [user, setUser] = useState('')
    const [open, setOpen] = useState(false)
    const [dialogText, setDialogText] = useState('')
    const [resultStatus, setResultStatus] = useState(false)

    const cars = useSelector(
        (state) => state.configureReducer.cars,
    );

    const batteries = useSelector(
        (state) => state.configureReducer.batteries,
    );

    const wheels = useSelector(
        (state) => state.configureReducer.wheels,
    );

    const tires = useSelector(
        (state) => state.configureReducer.tires,
    );

    const handleUser = (event) => {
        setUser(event.target.value.toLowerCase());
    }

    const handleClose = () => {
        setOpen(false)
    }

    const handleSaveButton = () => {
        if(price && user) {
            setOpen(true)
            setDialogText(`Dear: ${user}, car configuration costs ${price}`)
        }
    }

    const handleSave = () => {
    if(price && user){
        dispatch(save({username: user, price})).then((result) => {
            if(result){
                setResultStatus(true)
                setOpen(false)
            }
        })}
    }


    const handleChangeBattery = (event) => {
        let data = event.target.value;
        setBattery(data.name)
        setBatteryPrice(data.price)
        setWheelDisable(false)
        setWheelPrice(0)
        setWheel(null)
    }

    const handleChangeWheel = (event) => {
        let data = event.target.value;
        setWheel(data.name)
        setWheelPrice(data.price)
        setTireDisable(false)
        setTirePrice(0)
    }

    const handleChangeTire = (event) => {
        let data = event.target.value;
        setTirePrice(data.price)
    }

    useEffect(() => {
        if(cars[0]){
            setPrice(cars[0].price + batteryPrice +wheelPrice +tirePrice)

        }
        return () => {}
    }, [cars, batteryPrice, wheelPrice, tirePrice]);


    useEffect(() => {
        dispatch(fetchCar()).then(() => {});
        dispatch(fetchBattery()).then(() => {});
        return () => {}
    }, []);

    useEffect(() => {
        dispatch(fetchWheel(battery)).then(() => {});
        return () => {}
    }, [battery]);

    useEffect(() => {
        dispatch(fetchTire(wheel)).then(() => {});
        return () => {}
    }, [wheel]);

    return (
        <Box
            sx={{
                display: 'grid',
                gridTemplateColumns: 'repeat(1, 1fr)',
                gridTemplateRows: 'repeat(10, 1fr)',
                gridTemplateAreas: `"header" "main"`,
                width: '100%',
                height: '100%',
            }}
        >
            <Box sx={{ gridArea: 'header', gridRow: '1 / 3',marginTop: '20px'  }}>
                <>Car Configuration</>
                {resultStatus ? (
                <Alert variant="filled" severity="success">
                    This is a success alert — check it out!
                </Alert>) : <></>}
                <p>Price : {price}</p>
                <br/>
                <Box sx={{marginTop:'10px'}} >
                <TextField
                    fullWidth
                    color="primary"
                    label="Username"
                    id="username"
                    name="username"
                    variant="filled"
                    value={user}
                    onChange={handleUser}
                    required={true}
                />
                </Box>
                <br/>
            </Box>
            <Box sx={{ gridArea: 'main', gridRow: '3 / 10' }}>
                <Box>
                    <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {dialogText}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>Close</Button>
                            <Button onClick={handleSave} autoFocus>Agree</Button>
                        </DialogActions>
                    </Dialog>
                    <Box>
                    <FormControl fullWidth >
                        <InputLabel id="demo-simple-select-label">Battery</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={battery}
                            label="Battery"
                            onChange={handleChangeBattery}
                        >
                            {batteries &&  batteries.map((item, idx) => (
                                <MenuItem key={idx} value={item}>
                                    {item.name}
                                </MenuItem>
                            ))}

                        </Select>
                    </FormControl>
                    </Box>
                    <Box sx={{marginTop:'10px'}}>
                    <FormControl fullWidth disabled={wheelDisable}>
                        <InputLabel id="demo-simple-select-label">Wheel</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={battery}
                            label="Age"
                            onChange={handleChangeWheel}
                        >
                            {wheels &&  wheels.map((item, idx) => (
                                (item.active ? ( <MenuItem key={idx} value={item}>
                                    {item.name}
                                </MenuItem>): null )

                            ))}
                        </Select>
                    </FormControl>
                    </Box>
                    <Box sx={{marginTop:'10px'}} >
                    <FormControl fullWidth disabled={tireDisable}>
                        <InputLabel id="demo-simple-select-label">Tires</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={battery}
                            label="Age"
                            onChange={handleChangeTire}
                        >
                            {tires && tires.map((item, idx) => (
                                (item.active ? ( <MenuItem key={idx} value={item}>
                                    {item.name}
                                </MenuItem>): null )
                            ))}
                        </Select>
                    </FormControl>
                </Box>
                </Box>
                <Box sx={{marginTop:'10px'}} >
                <Button variant="outlined" fullWidth onClick={handleSaveButton}>
                    Save
                </Button>
                </Box>
            </Box>
        </Box>
    );
}