import React from 'react';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import { Paper } from '@mui/material';

export default function AppWrapper({ children }) {
    return (
        <Container
            component="main"
            maxWidth="md"
            fixed={true}
            sx={{ height: '100%', width: '100%'}}>
            <CssBaseline />
            <Paper
                elevation={0}
                square={true}
                sx={{ height: '100%', width: '100%'}}>
                {children}
            </Paper>
        </Container>
    );
}
