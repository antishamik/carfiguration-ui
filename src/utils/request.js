import axios from 'axios';

export const REQUEST = axios.create({
    baseURL: `${process.env.API_ROOT}`,
});