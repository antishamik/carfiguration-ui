import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import App from './containers/App';
import history from './utils/history';
import configureStore from "./redux/configureStore";
import {Provider} from "react-redux";

const initialState = {};
const store  = configureStore(initialState, history);
console.log("store", store)
ReactDOM.render(
    <Provider store={store}>
    <ConnectedRouter history={history}>
    <App />
    </ConnectedRouter>
    </Provider>,
  document.getElementById('root')
);
