const Dotenv = require('dotenv-webpack');


const { merge } = require('webpack-merge')
const commonConfig = require('./webpack.common.js');

module.exports = () => {
    const envConfig ={
        mode: 'production',
        // source-map - A full SourceMap is emitted as a separate file.
        // It adds a reference comment to the bundle so development tools know where to find it.
        devtool: 'source-map',
        plugins: [
            new Dotenv({
                path: './.env.production',
            })
        ],
    }
    // merge default configuration with a chosen mode configuration
    return merge(commonConfig, envConfig);
};
