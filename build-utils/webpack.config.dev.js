const Dotenv = require('dotenv-webpack');

const { merge } = require('webpack-merge')
const commonConfig = require('./webpack.common.js');

module.exports = () => {
    const envConfig = {
        mode: 'development',
        // eval-source-map - Each module is executed with eval() and a SourceMap
        // is added as a DataUrl to the eval(). Initially it is slow, but it provides
        // fast rebuild speed and yields real files. Line numbers are correctly mapped
        // since it gets mapped to the original code.
        // It yields the best quality SourceMaps for development.
        devtool: 'eval-source-map',
        plugins: [
            new Dotenv({
                path: './.env.development',
            })
        ],
    }
    // merge default configuration with a chosen mode configuration
    return merge(commonConfig, envConfig);
};


